import React from 'react';
import { Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';

const App = ({ navigation }) => {
  const handleOrder = () => {
    // Aquí puedes agregar la lógica para realizar un pedido.
    // Puedes mostrar una alerta, enviar una solicitud a un servidor, etc.
    // En este caso, se redirige a otra ventana llamada 'OrderScreen'
    navigation.navigate('Menu');
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Tostadas</Text>
      <Image source={require('../assets/tostadas.jpg')} style={styles.image} />
      <Text style={styles.subtitle}>Ingredientes:</Text>
      <Text style={styles.ingredients}>
        - Pan de tostadas{'\n'}
        - Aguacate{'\n'}
        - Tomate{'\n'}
        - Cebolla roja{'\n'}
        - Limón{'\n'}
        - Sal y pimienta al gusto
      </Text>
      <TouchableOpacity style={styles.button} onPress={handleOrder}>
        <Text style={styles.buttonText}>Hacer Pedido</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  image: {
    width: 300,
    height: 200,
    resizeMode: 'cover',
    marginBottom: 10,
    borderRadius: 10,
  },
  subtitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  ingredients: {
    fontSize: 16,
    marginBottom: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    marginTop: 20,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default App;
